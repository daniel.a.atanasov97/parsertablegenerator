#include <vector>
#include <queue>
#include <string>
#include <unordered_set>
#include <unordered_map>

#include <iostream>
#include <algorithm>

#define Set    std::unordered_set
#define Map    std::unordered_map
#define String std::string
#define Vector std::vector
#define Queue  std::queue
#define Pair   std::pair

template <typename T>
String ToString(T const& item)
{
    return item.ToString();
}

template <> String ToString(Set<String> const& set)
{
    String str = "{";
    for (int idx = 0; idx < set.size(); idx++)
    {
        if (idx) str += ", ";
        str += *std::next(std::begin(set), idx);
    }
    str += "}";
    return str;
}

template <> String ToString(Vector<String> const& vector)
{
    String str = "[";
    for (int idx = 0; idx < vector.size(); idx++)
    {
        if (idx) str += ", ";
        str += *std::next(std::begin(vector), idx);
    }
    str += "]";
    return str;
}

template <typename T, typename = void>
struct IsCoutableTrait : std::false_type {};
template <typename T>
struct IsCoutableTrait<T, std::void_t<decltype(std::cout << (std::declval<T>()))>> : std::true_type {};

template <typename T>
constexpr bool IsCoutable()
{
    return IsCoutableTrait<T>::value;
}

template <typename T>
void Print(T const& first)
{
    if constexpr (IsCoutable<T>())
    {
        std::cout << first; 
    }
    else
    {
        std::cout << ToString(first);
    }
}

template <typename T1, typename T2, typename ...Ts>
void Print(T1 first, T2 second, Ts... args)
{
    Print(std::forward<T1>(first));
    Print(' ');
    Print(std::forward<T2>(second), std::forward<Ts>(args)...);
}

template <typename ...Ts>
void PrintLn(Ts... args)
{
    Print(std::forward<Ts>(args)..., std::endl<char, std::char_traits<char>>);
}

class ClosureItem
{
private:
    String name_;
    Vector<String> rule_;
    Set<String> lookahead_;
    int position_;

public:
    ClosureItem(String name, Vector<String> rule, Set<String> lookahead, int position = 0) :
        name_(std::move(name)),
        rule_(std::move(rule)),
        lookahead_(std::move(lookahead)),
        position_(position)
    {
    }

    String const& Name() const
    {
        return name_;
    }
    
    Vector<String> const& Rule() const
    {
        return rule_;
    }

    Set<String> const& Lookahead() const
    {
        return lookahead_;
    }
    
    int Position() const
    {
        return position_;
    }

    String const& operator[](int idx) const
    {
        return rule_[idx];
    }

    int Size() const
    {
        return (int)rule_.size();
    }

    bool MatchesLR0(ClosureItem const& other) const
    {
        return Name() == other.Name() && Rule() == other.Rule();
    }

    void AddLookahead(Set<String> const& lookahead)
    {
        lookahead_.insert(lookahead.begin(), lookahead.end());
    }
    
    String ToString() const
    {
        String result = name_ + " :=";
        for (int idx = 0; idx < rule_.size(); idx++)
        {
            if (idx == Position()) result += " .";
            result += ' ' + rule_[idx];
        }
        return result + ", " + ::ToString(lookahead_);
    }

    bool operator==(ClosureItem const& other) const
    {
        return Name() == other.Name() && Rule() == other.Rule() && Lookahead() == other.Lookahead();
    }
};

class Closure
{
private:
    Vector<ClosureItem> items_;
    
public:
    Closure() = default;
    
    Vector<ClosureItem> const& Items() const
    {
        return items_;
    }

    ClosureItem const& operator[](int idx) const
    {
        return items_[idx];
    }

    int Size() const
    {
        return (int)items_.size();
    }

    bool MatchesLR0(Closure const& other) const
    {
        if (Size() != other.Size()) return false;
        for (int idx = 0; idx < Size(); idx++)
        {
            if (!items_[idx].MatchesLR0(other[idx])) return false;
        }
        return true;
    }
    
    void Add(ClosureItem const& item)
    {
        for (ClosureItem & it : items_)
        {
            if (item.Name() == it.Name() && item.Rule() == it.Rule())
            {
                it.AddLookahead(item.Lookahead());
                return;
            }
        }
        items_.push_back(item);
    }

    void Add(Closure const& closure)
    {
        for (ClosureItem const& item : closure.Items())
        {
            Add(item);
        }
    }
    
    String ToString() const
    {
        String result;
        for (int idx = 0; idx < items_.size(); idx++)
        {
            if (idx) result += "\n";
            result += items_[idx].ToString();
        }
        return result;
    }

    bool operator==(Closure const& other) const
    {
        return Items() == other.Items();
    }
};

class Parser
{
private:
    Map<String, Vector<Vector<String>>> rules_;
    Map<String, Set<String>> first_set_;

    Set<String> nonterminals_;
    Set<String> terminals_;
    
public:
    Map<String, Vector<Vector<String>>> const& Rules() const
    {
        return rules_;
    }
    
    Vector<Vector<String>> & operator[](String const& name)
    {
        return rules_[name];
    }

    Vector<Vector<String>> const& operator[](String const& name) const
    {
        return rules_.at(name);
    }

    bool IsNonterminal(String const& name) const
    {
        return nonterminals_.count(name);
    }

    void FillSymbolSets()
    {
        for (auto const& pair : rules_)
        {
            if (terminals_.count(pair.first)) terminals_.erase(pair.first);
            nonterminals_.insert(pair.first);

            for (Vector<String> const& rule : pair.second)
            {
                for (String const& symbol : rule)
                {
                    if (nonterminals_.count(symbol)) continue;
                    terminals_.insert(symbol);
                }
            }
        }
    }

    Set<String> const& Nonterminals() const
    {
        return nonterminals_;
    }

    Set<String> const& Terminals() const
    {
        return terminals_;
    }
    
    void FillFirstSets()
    {
        for (auto const& pair : rules_)
        {
            Set<String> first_set;
            Set<String> used {pair.first};
            Queue<String> queue;
            queue.push(pair.first);
            while (!queue.empty())
            {
                String current = queue.front();
                queue.pop();
        
                if (IsNonterminal(current))
                {
                    for (auto const& rule : rules_[current])
                    {
                        String const& first = rule.front();
                        if (!used.count(first))
                        {
                            used.insert(first);
                            queue.push(first);
                        }
            
                        for (String const& symbol : rule)
                        {
                            if (!IsNonterminal(symbol))
                            {
                                first_set_[symbol] = {symbol};
                            }
                        }
                    }
                }
                else
                {
                    first_set.insert(current);
                }
            }
            first_set_[pair.first] = first_set;
        }
    }

    Set<String> const& FirstSet(String const& name) const
    {
        return first_set_.at(name);
    }

    int RuleIndex(String const& name, Vector<String> const& rule) const
    {
        int idx;
        for (auto const& pair : rules_)
        {
            if (pair.first != name) continue;
            
            for (Vector<String> const& it : pair.second)
            {
                if (it == rule) return idx;
                idx++;
            }
        }
        return -1;
    }

    Closure ExpandClosure(String const& name, Set<String> const& lookahead = {"$"}) const
    {
        Closure closure;

        Vector<Pair<String, Set<String>>> used {{name, lookahead}};
        Queue<Pair<String, Set<String>>> queue;
        queue.push({name, lookahead});
        while (!queue.empty())
        {
            Pair<String, Set<String>> current = queue.front();
            queue.pop();
        
            Vector<Vector<String>> const& rules = rules_.at(current.first);
            for (Vector<String> const& rule : rules)
            {
                closure.Add({current.first, rule, current.second});

                if (!IsNonterminal(rule[0])) continue;

                Set<String> next_lookahead;
                bool not_last = rule.size() > 1;
                if (not_last) next_lookahead = FirstSet(rule[1]);
                else          next_lookahead = current.second;

                Pair<String, Set<String>> next {rule[0], std::move(next_lookahead)};
                if (std::find(used.begin(), used.end(), next) == used.end())
                {
                    used.push_back(next);
                    queue.push(next);
                }
            }
        }

        return closure;
    }

    Closure ShiftSymbol(Closure const& closure, String const& symbol) const
    {
        Closure result;
        for (ClosureItem const& item : closure.Items())
        {
            int idx = item.Position();
            if (idx == item.Size()) continue;

            String const& current = item[idx];
            if (current != symbol) continue;

            ClosureItem next_item(item.Name(), item.Rule(), item.Lookahead(), item.Position() + 1);

            int next_idx = idx + 1;
            if (next_idx == item.Size())
            {
                result.Add(next_item);
                continue;
            }

            String const& next = item[next_idx];
            result.Add(next_item);
        
            if (!IsNonterminal(next)) continue;

            Set<String> next_lookahead;
            if (next_idx + 1 != item.Size()) next_lookahead = FirstSet(item[next_idx + 1]);
            else                             next_lookahead = item.Lookahead();
            result.Add(ExpandClosure(next, next_lookahead));
        }
        return result;
    }
    
    Vector<Map<String, int>> GetStates(String const& start_symbol, bool lalr = true)
    {
        FillSymbolSets();
        FillFirstSets();
        
        Vector<Map<String, int>> actions;
    
        Closure start_closure = ExpandClosure(start_symbol);
        Vector<Closure> used {start_closure};
        Queue<Closure> queue;
        queue.push(start_closure);
        while (!queue.empty())
        {
            Closure current = queue.front();
            int current_idx = std::distance(used.begin(), std::find(used.begin(), used.end(), current));
            queue.pop();

            Set<String> shifted_symbols;
            for (ClosureItem const& item : current.Items())
            {
                int idx = item.Position();
                if (idx != item.Size())
                {
                    // Shift
                    String const& symbol = item[idx];
                    if (shifted_symbols.count(symbol)) continue;

                    shifted_symbols.insert(symbol);

                    int next_state;

                    Closure closure = ShiftSymbol(current, symbol);

                    std::function<bool(Closure const&)> clr_comparison = [&closure](Closure const& other)
                    {
                        return closure == other;
                    };
        
                    std::function<bool(Closure const&)> lalr_comparison = [&closure](Closure const& other)
                    {
                        return closure.MatchesLR0(other);
                    };
                    
                    auto it = std::find_if(used.begin(), used.end(), lalr ? lalr_comparison : clr_comparison);
                    if (it == used.end())
                    {
                        next_state = used.size();
                        used.push_back(closure);
                        queue.push(closure);
                    }
                    else
                    {
                        next_state = std::distance(used.begin(), it);
                        if (lalr) used[next_state].Add(closure);
                    }

                    if (actions.size() == current_idx) actions.resize(actions.size() + 1);

                    if (actions[current_idx].count(symbol) && actions[current_idx][symbol] < 0)
                    {
                        Print("reduce-reduce conflict");
                        return {};
                    }
                    actions[current_idx][symbol] = next_state;
                }
                else
                {
                    // Reduce
                    int idx = RuleIndex(item.Name(), item.Rule());
                    if (actions.size() == current_idx) actions.resize(actions.size() + 1);
                    for (String const& symbol : item.Lookahead())
                    {
                        if (actions[current_idx].count(symbol))
                        {
                            int value = actions[current_idx][symbol];
                            if      (value < 0) Print("reduce-reduce conflict");
                            else if (value > 0) Print("shift-reduce conflict");
                            return {};
                        }
                        actions[current_idx][symbol] = ~idx;
                    }
                }
            }
        }
        return actions;
    }
    
    String ToString() const
    {
        String result;
        for (auto const& pair : rules_)
        {
            for (Vector<String> const& rule : pair.second)
            {
                result += pair.first + " :=";
                for (String const& symbol : rule)
                {
                    result += ' ' + symbol; 
                }
                result += '\n';
            }
        }
        return result;
    }
};

int main()
{
    Parser parser;

    parser["START"] = {{"Disjunction"}};
    parser["Disjunction"] = {
        {"Disjunction", "Or", "Conjunction"},
        {"Conjunction"}
    };
    parser["Conjunction"] = {
        {"Conjunction", "And", "Sum"},
        {"Sum"}
    };
    parser["Sum"] = {
        {"Sum", "Plus", "Product"},
        {"Sum", "Minus", "Product"},
        {"Product"}
    };
    parser["Product"] = {
        {"Product", "Star", "Term"},
        {"Product", "Frontslash", "Term"},
        {"Term"}
    };
    parser["Term"] = {
        {"Identifier"},
        {"Number"},
        {"Minus", "Term"},
        {"Plus", "Term"},
        {"Not", "Term"},
        {"OpenParenthesis", "Disjunction", "CloseParenthesis"}
    };

    Vector<Map<String, int>> actions = parser.GetStates("START", false);

    for (String const& symbol : parser.Terminals())
    {
        Print(symbol);
        Print(",");
    }
    for (String const& symbol : parser.Nonterminals())
    {
        Print(symbol);
        Print(",");
    }
    PrintLn();
    for (Map<String, int> const& state : actions)
    {
        for (String const& symbol : parser.Terminals())
        {
            if (state.count(symbol))
            {
                int action = state.at(symbol);
                if (action > 0)
                {
                    Print("S", action);
                }
                else if (action < -1)
                {
                    Print("R", ~action);
                }
            }
            Print(",");
        }
    
        for (String const& symbol : parser.Nonterminals())
        {
            if (state.count(symbol))
            {
                Print("GOTO", state.at(symbol));
            }
            Print(",");
        }

        PrintLn();
    }

    PrintLn(actions.size());
    
    return 0;
}
